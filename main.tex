\documentclass[11pt,a4paper]{article}

\usepackage[latin1]{inputenc}
\usepackage[T1]{fontenc}
\usepackage{lscape}
\usepackage{fullpage}


\usepackage{tikz}
\usetikzlibrary{shapes}
\usetikzlibrary{fit} 
\usetikzlibrary{matrix}

\newcommand{\eg}{par exemple}

\title{Analyse statique de mod�les Matlab/Simulink~:\\
  vers une plate-forme logicielle coop�rative}

\author {A.C., O.B. et S.Z.}

\date{\today}

\begin{document}

\maketitle

\section{Contexte}
\label{sec:contexte}

La conception de syst�mes embarqu�s de contr�le-commande n�cessite une
phase de mod�lisation et de simulation de processus physiques
complexes avant la mise en {\oe}uvre. Cette phase est g�n�ralement
r�alis�e � l'aide d'outils de conception tels que Matlab/Simulink et
suit le mod�le dit de la conception fond�e sur la simulation, tr�s
utilis�e en automobile notamment. Ce mod�le s'appuie sur deux grandes
�tapes nomm�es~: 
\begin{itemize}
\item Model in The Loop (MTL)~: �tape permettant de d�finir les lois de
  commande de mani�re haut niveau en prenant en compte un mod�le du
  processus physique � commander. La loi de commande est d�finie dans le
  m�me formalisme que le processus physique (g�n�ralement des plances
  Simulink). La simulation num�rique permet de
  v�rifier/tester que la loi de commande correspond aux attentes.
\item Software in The Loop (STL)~: �tape v�rifiant par simulation
  num�rique que l'impl�menta\-tion de la loi de commande, \eg{} en
  langage C, correspond toujours aux attentes. A ce niveau,
  l'impl�mentation de la loi de commande interagit avec le mod�le du
  processus physiques: les deux sont d�crits dans des formalismes
  diff�rents.
\end{itemize}


L'analyse statique par interpr�tation abstraite est une m�thode de
v�rification formelle qui permet de prouver des propri�t�s sur les
programmes informatiques. Pour prouver ces propri�t�s, l'analyse
statique va g�n�ralement essayer de calculer des \emph{invariants} sur
le programme (i.e.  une formule logique qui doit �tre vraie pour toutes
les ex�cutions du programme) en effectuant une \emph{abstraction} du
processus physique, par exemple en ne gardant que les ranges de valeurs
admissibles pour chaque grandeur physique. Dans ce projet, nous nous
int�ressons particuli�rement aux invariants num�riques, c'est-�-dire des
invariants dont les formules logiques portent sur les valeurs associ�es
aux variables du programme. Le but de ce projet est l'application de ces
techniques sur les mod�les Matlab/Simulink utilis�s dans la phase MTL~:
ainsi, des erreurs de conception de la loi de contr�le-commande peuvent
�tre d�tect�es au plus t�t dans le cycle de d�veloppement. Pour mener �
bien ce projet, plusieurs verrous tant th�oriques que techniques devront
�tre lev�es, nous les d�taillons dans la suite de ce document.

\section{Point de d�part}
\label{sec:point-de-depart}

La conception d'un analyseur statique est complexe et elle est tr�s
consommatrice en temps. Nous comptons nous appuyer sur la plate forme
collaborative issue du projet ANR ASOPT (auquel certains auteur de ce
document ont particip�) pour d�velopper notre analyseur de mod�les
Matlab/Simulink. La plateforme ASOPT d�finit tous les
�l�ments n�cessaires � la conception et � l'utilisation d'un analyseur
statique de programmes C. En particulier elle se fonde sur~:
\begin{itemize}
\item un traducteur de C vers un langage interm�diaire~: Newpseak
\item une biblioth�que de domaines num�riques abstraits~: Apron
\item un solveur d'�quations s�mantiques~: Fixpoint
\end{itemize}
Ces �l�m�nts (repr�sent�s en rouge dans la
figure~\ref{fig:architecture-logicielle}) sont indispensable � la
conception d'un analyseur statique~: le programme initial est traduit en
Newspeak, puis le solveur Fixpoint tente de r�soudre les �quations
s�mantique g�n�r�es par le programme. Pour cela, Fixpoint utilise un
domaine abstrait choisi dans la biblioth�que APRON. Nous cherchons �
reproduire cette d�marche pour l'analyse de mod�les Simulink. 

\section{Objectifs et difficult�s th�oriques du projet}
\label{sec:objectifs-difficultes}

Cette section doit servir � d�finir clairement le but du projet et les
difficult�s auxquelles nous nous attendons. 

\subsection{Que veut dir ``analyser un mod�le Simulink'' ?}
\begin{itemize}
\item Analyse d'un programme~: bien connu et chemin balis� car
  s�mantique claire.
\item Simulink = outil de prototypage rapide et de simulation
\item pas de s�mantique formelle autre que celle de la simulation,
  langage tr�s riche (beaucoup de bo�tes)
\item S�mantique $\approx$ simulation, donc d�pend de param�tres
\item Mod�les h�t�rog�nes (temps discret/continu) $\Rightarrow$ m�me
  s�mantique pour les deux parties ?
\item Il faut donc se poser la question de la s�mantique des mod�les
  Simulink~: s�mantique parfaite (i.e. solution de l'ODE) ou s�mantique
  simul�e (i.e. simulation de l'ODE), les deux ?
\item La s�mantique de Simulink devra probablement faire intervenir une
  notion de temps qui n'est pas triviale~: temps discret ou temps
  continu ? Cette notion de temps demandera probablement des domaines
  abstraits sp�cifiques sensiblement diff�rents des domaines num�rique existant.
\item On pourra s'appuyer sur les travaux d'A. Chapoutot pour cette partie.
\end{itemize}


\subsection{Quel type d'analyse veut-on mener ?}
Une fois une s�mantique formelle bien d�finie, il faut se poser la
question des propri�t�s importantes que l'on veut pouvoir prouver sur
ces syst�mes. L� encore, la pr�sence du temps (continu ou discret) dans
la s�mantique de Simulink laisse � penser que les propri�t�s les plus
int�ressantes ne seront pas uniquement des invariants sur les variables
du mod�le mais peut-�tre des bornes sur l'�volution temporelle de ces
variables. Plusieurs analyses de syst�mes temporis�s ont d�j� �t�
men�es, il faut voir desquelles on souhaite s'inspirer~: 
\begin{itemize}
\item analyse de J. Bertrane.
\item analyse d'atteignabilit� en syst�me hybride.
\item preuve de formule LTL sur les automates temporis�s.
\end{itemize}


\subsection{Quel type de mod�le veut-on analyser ?}
Comme dit dans la Section~\ref{sec:contexte}, le langage Simulink est
tr�s large avec de nombreuses biblioth�ques sp�cialis�es dans certains
m�tiers. Nous devrons donc identifier un sous-ensemble de blocs Simulink
suffisemment riche pour permettre d'exprimer des lois de
contr�le-commande non triviales mais dont nous sommes capables
d'exprimer une s�mantique formelle. Ce sous-ensemble comprendra au
moins~:
\begin{itemize}
\item les blocs arithm�tiques,
\item les blocs int�grateur et retardateur,
\item le saturateur,
\item le bloc de zero-crossing detection.
\end{itemize}


\section{Vers l'analyse de Simulink~: une esquisse de plan de travail}
\label{sec:vers-l-analyse-de-simulink}

L'architecture logicielle d'un analyseur de Matlab/Simulink est donn�e
� la figure~\ref{fig:architecture-logicielle}. Elle s'appuie tr�s
largement sur la plate-forme Newpseak-Apron-Fixpoint. Les �l�ments en
rouge sont les �l�ments existant et les �l�ments en bleu sont les
�l�ments � ajouter.

Une suite au projet ANR ASOPT pourrait permettre la mise en {\oe}uvre
d'un analyseur statique de Matlab/Simulink. Les premi�res �l�ments de
r�flexion sont donn�s dans la suite.

\begin{figure}[t]
  \centering
  \begin{tikzpicture}[node distance=1.4cm]
    \tikzstyle{every node}=[draw, ultra thick];
    
    \node[ellipse, fill=green!20] (mdl) {mdl files};
    
    \node[ellipse, fill=green!20] (c_ada) [left of=mdl, node
    distance=6cm] {C/Ada files};
    
    \node[rectangle, dashed, fill=blue!20] (parser) [below of=mdl] {parser:
      Simulink to Newspeak};
    
    \node[rectangle, fill=red!20] (parser2) [below of=c_ada]
    {parser: C/Ada to Newspeak};
    
    \node[ellipse, dashed, fill=blue!20] (newspeakode) [below of=parser]
    {Newpseak with annoted ODEs};
    
    \node[rectangle, dashed, fill=blue!20] (pass2) [below of=newspeakode]
    {discretized state part};

    \node[rectangle, dashed, fill=blue!20, node distance=4cm] (pass1) [left
    of=pass2] {output part};

    \node[rectangle, dashed, fill=blue!20, node distance=5cm] (pass3)
    [right of=pass2] {guaranteed state part};
    
    \node[ellipse, fill=red!20] (newspeak) [below of=pass2]
    {Newspeak};

    \node[rectangle, fill=red!20] (npk2syntax) [below of=newspeak]
    {parser: npk2syntax};

    \node[ellipse, fill=red!20] (syntax) [below of=npk2syntax]
    {syntax};
    
    \node[rectangle, dashed, fill=blue!20] (hyfixpoint) [below
    of=syntax] {Hybrid Fixpoint};

    \node[rectangle, dashed, fill=blue!20] (hyapron) [left
    of=hyfixpoint, node distance=4cm] {Sequence domain};

    \node[rectangle, thick, fill=red!20] (fixpoint) [below
    of=hyfixpoint] {Fixpoint};
    
    \node[rectangle, thick, fill=red!20] [left of=fixpoint,
    node distance=4cm] (apron) {Apron};

    \node[rectangle, dashed, fill=blue!20] [right of=fixpoint,
    node distance=4cm] (acceleration) {Acceleration};

    \node[rectangle, thick, fill=red!20] [below of=fixpoint,
    node distance=1.5cm] (ptr) {Ptr};
    
    \draw[->] (mdl) -- (parser); 
    
    \draw[->] (parser) -- (newspeakode);

    \draw[->] (newspeakode) -- (pass1); 

    \draw[->] (newspeakode) -- (pass2); 

    \draw[->] (newspeakode) -- (pass3); 

    \draw[->] (pass1) -- (newspeak);

    \draw[->] (pass2) -- (newspeak);

    \draw[<->, dashed] (pass3) |- node[draw=none,right, near start] {GRKLib?}
    (hyfixpoint.east);

    \draw[->] (newspeak) -- (npk2syntax);

    \draw[->] (npk2syntax) -- (syntax);
    
    \draw[->] (syntax) -- (hyfixpoint);

    \draw[<->] (hyfixpoint) -- (fixpoint);

    \draw[<->] (hyapron) -- (apron);

    \draw[<->] (hyapron) -- (hyfixpoint);

    \draw[<->] (fixpoint) -- (apron);

    \draw[<->] (fixpoint) -- (ptr);

    \draw[<->] (fixpoint) -- (acceleration);

    \draw[<->] (hyfixpoint.base east) -| (acceleration);
    
    \draw[->] (c_ada) -- (parser2);

    \draw[->] (parser2) |- (newspeak);
  \end{tikzpicture}
  \caption{Architecture logicielle d'un analyseur de Matlab/Simulink}
  \label{fig:architecture-logicielle}
\end{figure}

\subsection{Traduction Simulink vers Newspeak}
\label{sec:traduction-simulink-vers-newspeak}

Les mod�les Matlab/Simulink m�langent des �quations diff�rentielles et
des �quations r�currentes pour mod�liser respectivement des syst�mes �
temps continu et des syst�mes � temps discret.

\textbf{Probl�matique}~: Newspeak a �t� con�u pour mod�liser des
programmes imp�ratifs �crits en C ou en Ada. Comment adapt� le langage
Newspeak pour d�crire des �quations diff�rentielles? Ou plus
g�n�ralement comment adapter Newspeak pour repr�senter l'environnement
physique d'un programme?

\subsection{Nouveaux domaines Apron}
\label{sec:nouveaux-domaines-apron}

Les mod�les Matlab/Simulink d�crivent l'�volution d'un syst�me
logiciel et environnement phy\-sique au cours du temps. L'objectif
d'une analyse statique de ces mod�les est alors d'inf�rer de nouveaux
types d'invariants permettant de caract�riser cette �volution
temporelle. Ces invariants pourrait �tre ainsi utilis�s pour la
v�rification fonctionnelle des syst�mes.

\textbf{Probl�matique}~: Apron est une biblioth�que de domaines
num�riques abstraits qui ne sont pas destin�s � la mod�lisation de
propri�t�s temporelles. Ajouter un nouveau domaine (ou une sur-couche
aux domaines) pour ce nouvel objectif tout en gardant les interfaces
existantes de Apron.

\subsection{Nouvel it�rateur de point fixe}
\label{sec:nouvel-iterateur-de-point-fixe}

Pour manipuler un domaine permettant d'inf�rer des propri�t�s
temporelles, il semble n�cessaire de modifier �galement la fa�on de
r�soudre les �quations s�mantiques.

\textbf{Probl�matique}~: Modifier Fixpoint (ou ajouter une sur-couche
� Fixpoint) pour inf�rer des propri�t�s temporelles.

\subsection{M�thodes d'acc�l�ration de la convergence}
\label{sec:methodes-d-acceleration-de-la-convergence}

La r�solution du calcul de point fixe peut �tre co�teuse en temps donc
il faut mettre en place des m�thodes pour acc�l�rer ce calcul.

\textbf{Probl�matique}~: Ajouter les m�thodes d'acc�l�ration par les
m�thodes num�riques dans le solveur Fixpoint et la biblioth�que Apron.

\end{document}
