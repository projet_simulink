TEX = pdflatex -shell-escape
ASPELL = /usr/local/GNU/bin/aspell

PAPER = main.tex

SRC = $(PAPER) 
AUX = $(SRC:.tex=.aux)
PDF = $(SRC:.tex=.pdf)

PART = 

TEX_COMP_FILE = *.aux *.log *.out *.gnuplot *.table *.dvi *.rel
BIB_COMP_FILE = *.bbl *.blg
ASPELL_BACKUP_FILE = *.bak

RES = collabortation.pdf

COR = main.tex

all: pdf

pdf : $(PDF)

res: $(RES)
	 cp $(PDF) $(RES)

.SUFFIXES: .tex .pdf 

.tex.pdf:
	@ $(TEX) $<
# 	bibtex $(AUX)
# 	@ $(TEX) $<
	@ $(TEX) $<

$(PDF) : $(PAPER) $(PART)

$(RES) : $(PDF)

or:
	$(ASPELL) -t --lang=en -c $(COR)

clean:
	rm -f *~ $(TEX_COMP_FILE) $(BIB_COMP_FILE) $(ASPELL_BACKUP_FILE)

distclean: clean
	rm -f $(PDF) $(RES)

.PHONY = or bib pdf clean distclean
